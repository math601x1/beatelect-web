
export const getSmallestImage = (imageObject: SpotifyApi.ImageObject[]) => {
  return imageObject.reduce((smallest, image) => {
    if ((image.height ?? 0) < (smallest.height ?? 0)) return image
    return smallest
  },
    imageObject[0]
  )
}