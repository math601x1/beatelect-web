import React, { useEffect, useMemo, useState } from 'react'
import { Box } from '@mui/material'
import { SearchBar } from '../components/SearchBar'
import { SessionInfo } from '../components/SessionInfo/SessionInfo'
import { PlaylistQueue } from '../components/PlaylistQueue'
import SearchResultMenu from '../components/SearchResultMenu/SearchResultMenu'
import { useSession } from '../hooks/session.hook'
import { PlayerMenu } from '../components/PlayerMenu'
import { useClient } from '../hooks/clients.hook'
import { Track } from '../models/SpotifyItems.model'
import { useSpotifyApi } from '../hooks/spotifyApi.hook'
import { useSocket } from '../hooks/socket.hook'
import { SOCKET_EVENTS } from '../config/socket.events'
import { getSmallestImage } from '../util/GetSmallestImage'
import { useAllTracks, useVisibleTracks } from '../hooks/tracks.hook'

const Session = () => {
  const [searchTerm, setSearchTerm] = useState<string>("")
  const session = useSession()
  const client = useClient()
  const spotifyApi = useSpotifyApi()
  const socket = useSocket()
  const playlistTracks = useAllTracks()
  const visiblePlaylistTracks = useVisibleTracks()

  useEffect(() => {
    if (session && client?.isSessionHost === false) {
      socket?.emit(SOCKET_EVENTS.CLIENT.GET_PLAYBACK_STATE)
    }
  }, [client?.isSessionHost])

  useEffect(() => {
    if (session && client?.isSessionHost && visiblePlaylistTracks && playlistTracks && visiblePlaylistTracks?.length === 1) {
      spotifyApi?.getRecommendations({
        limit: 2,
        seed_tracks: session?.playlistTracks.map(track => track.id)
      }).then(res => {
        spotifyApi.getTracks(res.body.tracks.map(track => track.id))
          .then(res => {
            spotifyApi.addTracksToPlaylist(session?.playlistId, res.body.tracks.map(track => track.uri))
            socket?.emit(SOCKET_EVENTS.CLIENT.NEW_TRACK_RECOMMENDATIONS, res.body.tracks.map(track => {
              return {
                id: track.id,
                artist: track.artists[0].name,
                artistId: track.artists[0].id,
                title: track.name,
                uri: track.uri,
                albumUrl: getSmallestImage(track.album.images).url,
                hasBeenPlayed: false,
                isRecommendation: true
              } as Track
            }))
          }
          ).catch(error => console.log(error))
      }).catch(error => console.log(error))
    }
  }, [visiblePlaylistTracks])

  const handleSetSearchInput = (input: string) => {
    setSearchTerm(input)
  }

  const clientImageUrls = useMemo(() => {
    return session?.clients.map(client => {
      return client.spotifyImageUrl
    })
  }, [session?.clients])

  return (
    <Box style={{ display: "flex", flexDirection: "column", justifyContent: "space-between", height: "97vh" }}>
      <Box>
        <SearchBar searchInput={searchTerm} setSearchInput={handleSetSearchInput} />
        <SessionInfo imageUrls={clientImageUrls ?? null} sessionKey={session?.sessionKey ?? ""} />
        <PlaylistQueue />
        {searchTerm !== "" ?
          <SearchResultMenu searchInput={searchTerm} setSearchInput={handleSetSearchInput} />
          : null
        }
      </Box>
      {client?.isSessionHost && visiblePlaylistTracks && visiblePlaylistTracks?.length > 0 ?
        <PlayerMenu />
        : null}
    </Box>
  )
}

export default Session