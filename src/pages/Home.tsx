import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { io } from 'socket.io-client'
import SpotifyWebApi from 'spotify-web-api-node'
import SessionSelectMenu from '../components/SessionSelectMenu/SessionSelectMenu'
import { apiUrl } from '../config/app.config'
import { authConfig } from '../config/auth.config'
import { SOCKET_EVENTS } from '../config/socket.events'
import { useSocketsContext } from '../context/socket.context'
import { useSpotifyContext } from '../context/spotify.context'
import { useAuth } from '../hooks/auth.hook'
import { Client } from '../models/Client.model'
import { Opinion } from '../models/Session.model'
import { stringNumbers } from '../util/StringNumbers'

const urlParams = new URLSearchParams(window.location.search)

const Home = () => {
  const accessToken = useAuth(urlParams.get('code') ?? null)
  const navigate = useNavigate()
  const socketContext = useSocketsContext()
  const spotifyContext = useSpotifyContext()

  useEffect(() => {
    if (!spotifyContext?.spotifyApi && !socketContext?.socket && accessToken) {
      console.log("setting spotify API")
      spotifyContext?.setSpotifyApi(new SpotifyWebApi({ clientId: authConfig.CLIENT_ID }))
      spotifyContext?.setCurrentAccessToken(accessToken)
      socketContext?.setSocket(io(apiUrl))
    }
  }, [accessToken, socketContext, spotifyContext])

  useEffect(() => {
    if (socketContext?.socket && spotifyContext?.spotifyApi?.getAccessToken()) {
      console.log("setting client")
      spotifyContext?.spotifyApi?.getMe().then(res => {
        const smallestImage = res.body.images?.reduce((smallest, image) => {
          if ((image.height ?? 0) < (smallest.height ?? 0)) return image
          return smallest
        },
          res.body.images[0]
        )

        const newClient = {
          socketId: socketContext?.socket.id,
          spotifyId: res.body.id,
          spotifyImageUrl: smallestImage?.url,
          trackOpinions: new Map<string, Opinion>(),
          isFollowingPlaylist: false
        } as Client

        socketContext?.setClient(newClient)
        socketContext?.socket.emit(SOCKET_EVENTS.CLIENT.SHARE_DETAILS, newClient)
      }, err => {
        console.log(err)
      }).catch(error => console.log(error))
    }
  }, [spotifyContext?.spotifyApi?.getAccessToken()])

  const handleCreateSession = () => {
    spotifyContext?.spotifyApi?.createPlaylist(`Beatelect Playlist ${stringNumbers(4)}`, {
      public: false,
      collaborative: true
    }).then(res => {
      const playlistId = res.body.id
      const playlistUri = res.body.uri
      spotifyContext.spotifyApi?.followPlaylist(playlistId)
      socketContext?.socket?.emit(SOCKET_EVENTS.CLIENT.CREATE_SESSION, playlistId, playlistUri)
      navigate("/session")
    }).catch(error => console.log(error))
  }

  const handleJoinSession = (keyInput: string) => {
    const sessionToJoin = socketContext?.sessions?.find(session => session.sessionKey === keyInput)

    if (sessionToJoin) {
      socketContext?.socket?.emit(SOCKET_EVENTS.CLIENT.JOIN_SESSION, sessionToJoin.id)
      navigate("/session")
    }
  }

  return (
    <SessionSelectMenu handleJoinSession={handleJoinSession} handleCreateSession={handleCreateSession} />
  )
}

export default Home