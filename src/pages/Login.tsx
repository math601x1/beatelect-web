import { Box, Button } from '@mui/material'
import { authConfig } from '../config/auth.config'

const Login = () => {
  const AUTH_URL = `${authConfig.AUTH_ENDPOINT}?client_id=${authConfig.CLIENT_ID}&redirect_uri=${authConfig.REDIRECT_URI}&scope=${authConfig.SCOPES}&response_type=code`

  return (
    <>
      <Box style={{ display: "flex", justifyContent: "center", marginTop: "300px" }}>
        <Button href={AUTH_URL} variant="contained">
          Login with spotify
        </Button>
      </Box>
    </>

  )
}

export default Login