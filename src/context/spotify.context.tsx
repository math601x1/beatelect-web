import React, { createContext, useContext, useEffect, useRef, useState } from 'react'
import SpotifyWebApi from 'spotify-web-api-node'

interface Context {
  spotifyApi: SpotifyWebApi | undefined,
  setSpotifyApi: (spotifyApi: SpotifyWebApi) => void,
  setCurrentAccessToken: (token: string) => void,
  currentAccessToken: React.MutableRefObject<string | undefined>
}

const SpotifyContext = createContext<Context | null>(null)

export const SpotifyApiProvider = (props: any) => {
  const [spotifyApi, setSpotifyApi] = useState<SpotifyWebApi>()
  const currentAccessToken = useRef<string>()

  const setCurrentAccessToken = (token: string) => {
    currentAccessToken.current = token
  }

  useEffect(() => {
    if (currentAccessToken.current) {
      spotifyApi?.setAccessToken(currentAccessToken.current)
    }
  }, [currentAccessToken, spotifyApi])

  return (
    <SpotifyContext.Provider value={{ spotifyApi, setSpotifyApi, currentAccessToken, setCurrentAccessToken }} {...props} />
  )
}

export const useSpotifyContext = () => useContext(SpotifyContext)