import { createContext, useContext, useEffect, useState } from "react"
import { Socket } from "socket.io-client"
import { SOCKET_EVENTS } from "../config/socket.events"
import { Session } from "../models/Session.model"
import { Client } from "../models/Client.model"
import { Track } from "../models/SpotifyItems.model"
import { useSpotifyApi } from "../hooks/spotifyApi.hook"

interface Context {
  socket: Socket,
  sessions: Session[],
  currentSession: Session,
  client: Client,
  setClient: (client: Client) => void,
  setSocket: (socket: Socket) => void,
  visiblePlaylistTracks: Track[],
  playbackState: boolean,
}

const SocketContext = createContext<Context | null>(null)

export const SocketsProvider = (props: any) => {
  const [currentSession, setCurrentSession] = useState<Session>()
  const [client, setClient] = useState<Client>()
  const [sessions, setSessions] = useState<Session[]>()
  const [socket, setSocket] = useState<Socket>()
  const [visiblePlaylistTracks, setVisiblePlaylistTracks] = useState<Track[]>()
  const [playbackState, setPlaybackState] = useState<boolean>(false)
  const [tracksToDelete, setTracksToDelete] = useState<number[]>([])
  const [dataReport, setDataReport] = useState<string>("")
  const spotifyApi = useSpotifyApi()
  const [tracksToAddToPlaylist, setTracksToAddToPlaylist] = useState<Track[]>()

  socket?.on(SOCKET_EVENTS.SERVER.UPDATE_SESSIONS, (updatedSessions: Session[]) => {
    setSessions(updatedSessions)
  })

  socket?.on(SOCKET_EVENTS.SERVER.UPDATE_SESSION, (updatedSession: Session) => {
    setCurrentSession(updatedSession)
  })

  socket?.on(SOCKET_EVENTS.SERVER.UPDATE_PLAYBACK_STATE, (isPlayingState: boolean) => {
    setPlaybackState(isPlayingState)
  })

  socket?.on(SOCKET_EVENTS.SERVER.REMOVE_ARTIST_TRACKS, (trackIndices: number[]) => {
    setTracksToDelete(trackIndices)
  })

  socket?.on(SOCKET_EVENTS.SERVER.CLIENT_DISCONNECT, (dataReport: string) => {
    setDataReport(dataReport)
  })

  socket?.on(SOCKET_EVENTS.SERVER.ADD_TRACKS_TO_PLAYLIST, (tracks: Track[]) => {
    setTracksToAddToPlaylist(tracks)
  })

  useEffect(() => {
    if (currentSession?.playlistId && tracksToAddToPlaylist) {
      console.log("adding tracks" + tracksToAddToPlaylist)
      spotifyApi?.addTracksToPlaylist(currentSession?.playlistId, tracksToAddToPlaylist.map(track => track.uri
      )).then(res => {
        socket?.emit(SOCKET_EVENTS.CLIENT.HAS_ADDED_TRACKS_TO_PLAYLIST, (tracksToAddToPlaylist))
      }).catch(error => console.log(error))
    }
  }, [tracksToAddToPlaylist])

  useEffect(() => {
    tracksToDelete.forEach(index => {
      console.log("removing track" + currentSession?.playlistTracks[index].title)
    })
    if (currentSession?.playlistId) {
      spotifyApi?.getPlaylist(currentSession?.playlistId)
        .then(res => {
          spotifyApi?.removeTracksFromPlaylistByPosition(currentSession?.playlistId, tracksToDelete, res.body.snapshot_id)
            .catch(error => console.log(error))
        })
        .catch(error => console.log(error))
    }
  }, [tracksToDelete])

  useEffect(() => {
    if (currentSession && client?.isSessionHost !== true) {
      const checkIsSessionHost = client?.socketId === currentSession.sessionHost.socketId
      setClient(client => ({
        ...client,
        isSessionHost: checkIsSessionHost,
        isFollowingPlaylist: checkIsSessionHost
      } as Client))
    }
  }, [currentSession])

  useEffect(() => {
    if (dataReport) {
      console.log(dataReport)
    }
  }, [dataReport])

  useEffect(() => {
    if (currentSession?.playlistTracks) {
      let nextVisibleTracks = [] as Track[]
      nextVisibleTracks = nextVisibleTracks.concat(currentSession.playlistTracks.slice(currentSession.currentPlayingTrackIndex, currentSession.currentPlayingTrackIndex + 7))
      setVisiblePlaylistTracks(nextVisibleTracks)
    }
  }, [currentSession?.currentPlayingTrackIndex, currentSession?.playlistTracks])

  return (
    <SocketContext.Provider value={{ socket, setSocket, sessions, currentSession, client, setClient, visiblePlaylistTracks, playbackState }} {...props} />
  )
}

export const useSocketsContext = () => useContext(SocketContext)