import { useSocketsContext } from "../context/socket.context"

export const useSessionClients = () => {
  const socketsContext = useSocketsContext()
  return socketsContext?.currentSession.clients
}

export const useClient = () => {
  const socketsContext = useSocketsContext()
  return socketsContext?.client
}

export const useHost = () => {
  const socketsContext = useSocketsContext()
  return socketsContext?.currentSession.sessionHost
}