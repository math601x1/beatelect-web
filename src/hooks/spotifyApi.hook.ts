import { useSpotifyContext } from "../context/spotify.context"

export const useSpotifyApi = () => {
  const spotifyContext = useSpotifyContext()
  return spotifyContext?.spotifyApi
}