import { useSocketsContext } from "../context/socket.context"

export const useSocket = () => {
  const socketsContext = useSocketsContext()
  return socketsContext?.socket
}