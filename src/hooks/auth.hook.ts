import { useState, useEffect } from "react"
import axios from "axios"
import { authConfig } from "../config/auth.config"

export const useAuth = (code: string | null) => {
  const [accessToken, setAccessToken] = useState<string>()
  const [refreshToken, setRefreshToken] = useState<string>()
  const [expiresIn, setExpiresIn] = useState<number>()

  useEffect(() => {
    if (code !== null) {
      axios.post(authConfig.AUTH_URLS.AUTH_TOKEN_GRANT, {
        code,
      })
        .then(res => {
          setAccessToken(res.data.accessToken)
          setRefreshToken(res.data.refreshToken)
          setExpiresIn(res.data.expiresIn)
          window.history.pushState({}, '', '/home')
        })
        .catch((err) => {
          console.log(err)
          window.location.href = '/'
        })
    }
  }, [code])

  useEffect(() => {
    if (refreshToken && expiresIn) {
      const interval = setInterval(() => {
        axios.post(authConfig.AUTH_URLS.AUTH_TOKEN_SWAP, {
          refreshToken,
        })
          .then(res => {
            setAccessToken(res.data.accessToken)
            setExpiresIn(res.data.expiresIn)
          })
          .catch((err) => {
            console.log(err)
            window.location.href = '/'

          })
      }, (1000 * 60 * expiresIn))

      return () => clearInterval(interval)
    }
  }, [refreshToken, expiresIn])

  return accessToken
}