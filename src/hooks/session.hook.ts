import { useSocketsContext } from "../context/socket.context"

export const useSession = () => {
  const socketsContext = useSocketsContext()
  return socketsContext?.currentSession
}