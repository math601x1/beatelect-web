import { useSocketsContext } from "../context/socket.context"

export const useAllTracks = () => {
  const socketContext = useSocketsContext()
  return socketContext?.currentSession?.playlistTracks
}

export const useVisibleTracks = () => {
  const socketContext = useSocketsContext()
  return socketContext?.visiblePlaylistTracks
}

export const usePlaybackState = () => {
  const socketContext = useSocketsContext()
  return socketContext?.playbackState
}