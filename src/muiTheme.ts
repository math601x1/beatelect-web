import { createTheme } from "@mui/material";
import { text } from "stream/consumers";

const appTheme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#F8804A"
    },
    secondary: {
      main: "rgba(255, 255, 255, 0.8)"
    }
  }
})

export default appTheme