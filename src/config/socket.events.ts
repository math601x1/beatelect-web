export const SOCKET_EVENTS = {
  CONNECT: "connect",
  DISCONNECT: "disconnect",
  CLIENT: {
    SHARE_DETAILS: "share_details",
    CREATE_SESSION: "create_session",
    JOIN_SESSION: "join_session",
    ADD_TRACKS_TO_PLAYLIST: "add_tracks_to_playlist",
    HAS_ADDED_TRACKS_TO_PLAYLIST: "has_added_tracks_to_playlist",
    UPDATE_CURRENTLY_PLAYING_TRACK: "client_update_currently_playing_track",
    NEW_TRACK_RECOMMENDATIONS: "new_track_recommendations",
    UPDATE_PLAYBACK_STATE: "client_update_playback_state",
    GET_PLAYBACK_STATE: "get_playback_state",
    LIKE_TRACK: "like_track",
    HAS_LIKED_TRACK: "has_liked_track",
    DISLIKE_TRACK: "dislike_track",
  },
  SERVER: {
    UPDATE_SESSIONS: "update_sessions",
    UPDATE_SESSION: "update_session",
    UPDATE_PLAYBACK_STATE: "server_update_playback_state",
    REMOVE_ARTIST_TRACKS: "server_remove_artist_tracks",
    CLIENT_DISCONNECT: "server_client_disconnect",
    ADD_TRACKS_TO_PLAYLIST: "server_add_tracks_to_playlist",
    LIKE_TRACK: "server_like_track",
  }
}