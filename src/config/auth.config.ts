import { rootUrl } from "./app.config";

export const authConfig = {
  AUTH_ENDPOINT: 'https://accounts.spotify.com/authorize',
  CLIENT_ID: '029742eff7394f3e95b820067bdf1e66',
  REDIRECT_URI: rootUrl + '/home',
  SCOPES: [
    'user-read-email',
    'user-read-private',
    'user-read-playback-state',
    'user-modify-playback-state',
    'playlist-read-private',
    'playlist-read-collaborative',
    'playlist-modify-public',
    'playlist-modify-private',
    'streaming',
  ].join('%20'),
  AUTH_URLS: {
    AUTH_TOKEN_GRANT: 'https://beatelect.m9ssen.me:8443/api/accessTokenGrant',
    AUTH_TOKEN_SWAP: 'https://beatelect.m9ssen.me:8443/api/refreshTokenSwap'
  }
}