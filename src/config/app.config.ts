const rootUrls = {
  production: 'https://main.d3kosm6oqdbkkr.amplifyapp.com',
  development: 'http://localhost:8443'
};

const apiUrls = {
  production: 'https://beatelect.m9ssen.me:8443',
  development: 'https://beatelect.m9ssen.me:8443'
}

export var rootUrl: string
export var apiUrl: string

if (window.location.hostname === 'localhost') {
  rootUrl = rootUrls.development
  apiUrl = apiUrls.development
} else {
  rootUrl = rootUrls.production
  apiUrl = apiUrls.production
}