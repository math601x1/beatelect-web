import { Box } from '@mui/material'
import React, { useEffect, useState } from 'react'
import SpotifyWebPlayer, { CallbackState } from 'react-spotify-web-playback/lib'
import { SOCKET_EVENTS } from '../config/socket.events'
import { useSpotifyContext } from '../context/spotify.context'
import { useSession } from '../hooks/session.hook'
import { useSocket } from '../hooks/socket.hook'

export const PlayerMenu = () => {
  const [currentlyPlayingTrackId, setCurrentlyPlayingTrackId] = useState<string>()
  const [isPlaying, setIsPlaying] = useState<boolean>(false)
  const spotifyContext = useSpotifyContext()
  const session = useSession()
  const socket = useSocket()

  useEffect(() => {
    if (currentlyPlayingTrackId) {
      socket?.emit(SOCKET_EVENTS.CLIENT.UPDATE_CURRENTLY_PLAYING_TRACK, currentlyPlayingTrackId)
    }
  }, [currentlyPlayingTrackId])

  useEffect(() => {
    socket?.emit(SOCKET_EVENTS.CLIENT.UPDATE_PLAYBACK_STATE, isPlaying)
  }, [isPlaying])

  const handleCallback = ({ type, ...state }: CallbackState) => {
    setCurrentlyPlayingTrackId(state.track.id)
    setIsPlaying(state.isPlaying)
  }

  return (
    <Box style={{ fontFamily: "sans-serif", zIndex: "0" }}>
      {spotifyContext?.currentAccessToken.current ?
        <SpotifyWebPlayer
          token={spotifyContext.currentAccessToken.current}
          initialVolume={0.4}
          syncExternalDevice={true}
          callback={handleCallback}
          styles={{
            activeColor: '#fff',
            bgColor: '#333',
            color: '#fff',
            loaderColor: '#fff',
            sliderColor: '#F8804A',
            trackArtistColor: '#fff',
            trackNameColor: '#fff',
          }}
        />
        : null}
    </Box >
  )
}