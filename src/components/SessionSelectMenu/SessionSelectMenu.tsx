import { Box, Button, TextField, Typography } from '@mui/material'
import React, { useState } from 'react'
import "./SessionSelectMenu.css"

interface Props {
  handleJoinSession: (keyInput: string) => void,
  handleCreateSession: () => void,
}

const SessionSelectMenu = (props: Props) => {
  const { handleJoinSession, handleCreateSession } = props
  const [keyInput, setKeyInput] = useState<string>("")

  return (
    <Box className={"content-container"}>
      <Box className={"sessionSelectMenu-container"}>
        <Box style={{ margin: "5px" }}>
          <form className={"sessionSelectMenu-form"} onSubmit={e => {
            e.preventDefault()
            handleJoinSession(keyInput)
          }}>
            <TextField
              fullWidth={true}
              type={"text"}
              label={"Enter a session key"}
              size={"small"}
              onChange={e => setKeyInput(e.target.value)}
            />
            <Button
              variant={"contained"}
              type={"submit"}
              style={{ marginLeft: "5px", width: "40%" }}
            >
              Join Session
            </Button>
          </form>
        </Box>
        <Typography style={{ margin: "auto" }} color={"secondary"}>
          Or
        </Typography>
        <Button onClick={handleCreateSession} variant={"outlined"} style={{ margin: "5px" }}>
          Create a new Session
        </Button>
      </Box>
    </Box>
  )
}

export default SessionSelectMenu