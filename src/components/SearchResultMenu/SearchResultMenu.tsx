import { Box, Typography, Paper, Card } from "@mui/material";
import React, { useEffect, useState } from 'react'
import "./SearchResultMenu.css"
import { useSpotifyApi } from "../../hooks/spotifyApi.hook";
import { useSession } from "../../hooks/session.hook";
import { useSocket } from "../../hooks/socket.hook";
import { SOCKET_EVENTS } from "../../config/socket.events";
import { Track, Artist, Playlist } from "../../models/SpotifyItems.model";
import { truncateNameString } from "../../util/TruncateNameString";
import { getSmallestImage } from "../../util/GetSmallestImage";
import { stringNumbers } from "../../util/StringNumbers";
import { useSocketsContext } from "../../context/socket.context";

interface Props {
  setSearchInput: (input: string) => void,
  searchInput: string,
}

const SearchResultMenu = (props: Props) => {
  const { searchInput, setSearchInput } = props
  const [trackSearchResult, setTrackSearchResult] = useState<Track[]>()
  const [artistSearchResult, setArtistSearchResult] = useState<Artist[]>()
  const [playlistSearchResult, setPlaylistSearchResult] = useState<Playlist[]>()
  const spotifyApi = useSpotifyApi()
  const session = useSession()
  const socket = useSocket()

  const isOnMobile = window.screen.width < 1200
  const truncatedStringWidth = isOnMobile ? 24 : 30

  const addTracksToPlaylist = (tracks: Track[]) => {
    console.log("sending tracks to server" + tracks)
    socket?.emit(SOCKET_EVENTS.CLIENT.ADD_TRACKS_TO_PLAYLIST, tracks)
    setSearchInput('')
  }

  const handleAddTopArtistTracks = (artistId: string) => {
    if (session?.playlistId) {
      spotifyApi?.getArtistTopTracks(artistId, "DK")
        .then(res => {
          addTracksToPlaylist(
            res.body.tracks?.map(track => {
              return {
                id: track.id,
                artist: track.artists[0].name,
                artistId: track.artists[0].id,
                title: track.name,
                uri: track.uri,
                albumUrl: getSmallestImage(track.album.images)?.url,
                hasBeenPlayed: false
              } as Track
            }))
        }).catch(error => console.log(error))
    }
  }

  const handleAddPlaylistTracksToPlaylist = (playlistId: string) => {
    if (session?.playlistId) {
      spotifyApi?.getPlaylistTracks(playlistId)
        .then(res => {
          addTracksToPlaylist(
            res.body.items?.map(item => {
              return {
                id: item.track.id,
                artist: item.track.artists[0].name,
                artistId: item.track.artists[0].id,
                title: item.track.name,
                uri: item.track.uri,
                albumUrl: getSmallestImage(item.track.album.images)?.url,
                hasBeenPlayed: false
              } as Track
            })
          )
        }
        ).catch(error => console.log(error))
    }
  }

  useEffect(() => {
    if (searchInput) {
      spotifyApi?.searchPlaylists(searchInput, { limit: 6 })
        .then(res => {
          setPlaylistSearchResult(
            res.body.playlists?.items.map(playlist => {
              return {
                id: playlist.id,
                playlistName: playlist.name,
                authorName: playlist.owner.display_name,
                imageUrl: getSmallestImage(playlist.images)?.url,
                uri: playlist.uri
              } as Playlist
            })
          )
        }).catch(error => console.log(error))
    }
  }, [searchInput, spotifyApi])

  useEffect(() => {
    if (searchInput) {
      spotifyApi?.searchArtists(searchInput, { limit: 3 })
        .then(res => {
          setArtistSearchResult(
            res.body.artists?.items.map(artist => {
              return {
                id: artist.id,
                artistName: artist.name,
                imageUrl: getSmallestImage(artist.images)?.url,
              } as Artist
            })
          )
        }).catch(error => console.log(error))
    }
  }, [searchInput, spotifyApi])

  useEffect(() => {
    if (searchInput) {
      spotifyApi?.searchTracks(searchInput)
        .then(res => {
          setTrackSearchResult(
            res.body.tracks?.items.map(track => {
              return {
                id: track.id,
                artist: track.artists[0].name,
                artistId: track.artists[0].id,
                title: track.name,
                uri: track.uri,
                albumUrl: getSmallestImage(track.album.images)?.url,
                hasBeenPlayed: false
              } as Track
            })
          )
        }).catch(error => console.log(error))
    }
  }, [searchInput, spotifyApi])

  return (
    <Paper className="search-result-menu">
      <Box className="search-result-container-top">
        {artistSearchResult && artistSearchResult?.length > 0 ?
          <Card variant="outlined" className="artist-search-result-card">
            <Typography variant="h5">
              Top Artists
            </Typography>

            {artistSearchResult?.map(artist => (
              <div onClick={e => handleAddTopArtistTracks(artist.id)} className="search-result-item" key={stringNumbers(5)}>
                <img src={artist.imageUrl} style={{ height: "64px", width: "64px", marginRight: "8px" }} alt="" />
                <Typography variant="h6">
                  {artist.artistName}
                </Typography>
              </div>
            ))}
          </Card>
          : null}

        {playlistSearchResult && artistSearchResult && playlistSearchResult.length > 0 ?
          <Card variant="outlined" className={artistSearchResult.length > 0 ? "playlist-search-result-card" : "playlist-search-result-card-fullwidth"}>
            <Typography variant="h5">
              Playlists
            </Typography>

            <Box className="playlist-items-container">
              {playlistSearchResult?.slice(0, 6).map(playlist => (
                <div onClick={e => handleAddPlaylistTracksToPlaylist(playlist.id)} className="search-result-item" key={stringNumbers(5)}>
                  <img src={playlist.imageUrl} style={{ height: "64px", width: "64px", marginRight: "8px" }} alt="" />
                  <Box>
                    <Typography variant="h6">
                      {truncateNameString(playlist.playlistName, truncatedStringWidth)}
                    </Typography>
                    <Typography>
                      {playlist.authorName}
                    </Typography>
                  </Box>
                </div>
              ))}
            </Box>
          </Card>
          : null}
      </Box>

      <Box className="search-result-container-bottom">
        {trackSearchResult && trackSearchResult.length > 0 ?
          <Card variant="outlined" className="track-search-result-card">
            <Typography variant="h5">
              Songs
            </Typography>

            {trackSearchResult?.map(track => (
              <div onClick={e => addTracksToPlaylist([track])} className="search-result-item" key={stringNumbers(5)}>
                <img src={track.albumUrl} style={{ height: "64px", width: "64px", marginRight: "8px" }} alt="" />
                <Box>
                  <Typography variant="h6">
                    {truncateNameString(track.title, isOnMobile ? 25 : 100)}
                  </Typography>
                  <Typography>
                    {truncateNameString(track.artist, truncatedStringWidth)}
                  </Typography>
                </Box>
              </div>
            ))}
          </Card>
          : null}
      </Box>
    </Paper>
  )
}

export default SearchResultMenu