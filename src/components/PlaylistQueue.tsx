import { Check } from '@mui/icons-material'
import { Box, Button, Paper, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { SOCKET_EVENTS } from '../config/socket.events'
import { useSocketsContext } from '../context/socket.context'
import { useClient } from '../hooks/clients.hook'
import { useSession } from '../hooks/session.hook'
import { useSocket } from '../hooks/socket.hook'
import { useSpotifyApi } from '../hooks/spotifyApi.hook'
import { usePlaybackState, useVisibleTracks } from '../hooks/tracks.hook'
import { Opinion } from '../models/Session.model'
import { Track } from '../models/SpotifyItems.model'
import { getSmallestImage } from '../util/GetSmallestImage'
import { stringNumbers } from '../util/StringNumbers'
import { PlaylistTrackItem } from './PlaylistTrackItem/PlaylistTrackItem'

export const PlaylistQueue = () => {
  const visiblePlaylistTracks = useVisibleTracks()
  const playbackState = usePlaybackState()
  const spotifyApi = useSpotifyApi()
  const session = useSession()
  const socket = useSocket()
  const client = useClient()
  const [isFollowingPlaylist, setIsFollowingPlaylist] = useState<boolean>()
  const [likedTrackId, setLikedTrackId] = useState<string>("")

  useEffect(() => {
    if (client?.isSessionHost) {
      setIsFollowingPlaylist(true)
    }
  }, [client?.isSessionHost])

  const handleFollowPlaylist = () => {
    if (session?.playlistId) {
      if (isFollowingPlaylist) {
        spotifyApi?.unfollowPlaylist(session?.playlistId)
        setIsFollowingPlaylist(false)
      } else {
        spotifyApi?.followPlaylist(session?.playlistId)
        setIsFollowingPlaylist(true)
      }
    }
  }

  socket?.on(SOCKET_EVENTS.SERVER.LIKE_TRACK, (trackId: string) => {
    setLikedTrackId(trackId)
  })

  useEffect(() => {
    if (session?.playlistId && visiblePlaylistTracks && spotifyApi) {
      let selectedTrackPosition = session.playlistTracks.findIndex(track => track.id === likedTrackId && track.hasBeenPlayed === false)
      if (selectedTrackPosition === -1) selectedTrackPosition = session?.currentPlayingTrackIndex

      spotifyApi?.getRecommendations({
        limit: 3,
        seed_tracks: [likedTrackId],
        seed_artists: [session.playlistTracks.find(track => track.id === likedTrackId)?.artistId ?? ""],
      }).then(res => {
        spotifyApi.getTracks(res.body.tracks.map(track => track.id))
          .then(res => {
            let isAlternating: boolean
            const timeIntervals = [2000, 4000, 6000]
            const positions = [] as number[]
            if (visiblePlaylistTracks.slice(selectedTrackPosition).length > 3) {
              for (let i = 1; i < 4; i++) {
                positions.push(selectedTrackPosition + (i * 2) - 1)
              }
              res.body.tracks.forEach(track => setTimeout(() => {
                spotifyApi.addTracksToPlaylist(session?.playlistId, [track.uri], { position: positions?.shift() })
              }, timeIntervals.shift()))
              isAlternating = true
            } else {
              spotifyApi.addTracksToPlaylist(session?.playlistId, res.body.tracks.map(track => track.uri))
              isAlternating = false
            }

            //add liked tracks
            socket?.emit(SOCKET_EVENTS.CLIENT.HAS_LIKED_TRACK, res.body.tracks.map(track => {
              return {
                id: track.id,
                artist: track.artists[0].name,
                artistId: track.artists[0].id,
                title: track.name,
                uri: track.uri,
                albumUrl: getSmallestImage(track.album.images).url,
                hasBeenPlayed: false,
                isRecommendation: true
              } as Track
            }), selectedTrackPosition, isAlternating)

          }).catch(error => console.log(error))
      }).catch(error => console.log(error))
    }
  }, [likedTrackId])

  const handleNewTrackOpinion = (trackId: string, newOpinion: Opinion) => {
    if (session?.playlistId && visiblePlaylistTracks) {
      let selectedTrackPosition = session.playlistTracks.findIndex(track => track.id === trackId && track.hasBeenPlayed === false)
      if (selectedTrackPosition === -1) selectedTrackPosition = session?.currentPlayingTrackIndex

      if (newOpinion === "likes") {
        socket?.emit(SOCKET_EVENTS.CLIENT.LIKE_TRACK, trackId)

      }

      else if (newOpinion === "dislikes") {
        socket?.emit(SOCKET_EVENTS.CLIENT.DISLIKE_TRACK, session.playlistTracks[selectedTrackPosition], selectedTrackPosition)
      }
    }
  }

  return (
    <Paper style={{ margin: "5px", padding: "2px" }}>
      <Box style={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h6" style={{ borderBottom: "1px solid rgba(0, 0, 0, 0.23)" }}>
          Play queue
        </Typography>
        <Button
          onClick={e => handleFollowPlaylist()}
          style={isFollowingPlaylist ? { color: "grey" } : {}}
          startIcon={
            isFollowingPlaylist ? <Check /> : null
          }>
          Follow playlist
        </Button>
      </Box>
      <Box style={{ display: "flex", flexDirection: "column" }}>
        {visiblePlaylistTracks?.map(track => (
          <div key={stringNumbers(5)}>
            <PlaylistTrackItem track={track} handleTrackOpinion={handleNewTrackOpinion} isCurrentlyPlaying={(visiblePlaylistTracks.indexOf(track) === 0 && playbackState === true) ?? false} />
          </div>
        ))}
      </Box>
    </Paper>
  )
}