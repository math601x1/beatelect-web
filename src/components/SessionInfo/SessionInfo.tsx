import { Box, Typography } from '@mui/material'
import React from 'react'
import { stringNumbers } from '../../util/StringNumbers'
import "./SessionInfo.css"

interface Props {
  imageUrls: string[] | null,
  sessionKey: string,
}

export const SessionInfo = (props: Props) => {
  const { imageUrls, sessionKey } = props

  return (
    <Box className={"sessionInfo-container"}>
      <Typography style={{ paddingTop: "6px" }}>
        {`Session key: ${sessionKey} | `}
      </Typography>
      <Box style={{ display: "flex" }}>
        {imageUrls?.map(url => (
          <div style={{ marginTop: "4px", marginLeft: "4px" }} key={stringNumbers(5)}>
            <img src={url} style={{ height: "32px", width: "32px", borderRadius: "50px" }} alt="" />
          </div>
        ))}
      </Box>
    </Box>
  )
}