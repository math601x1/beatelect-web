import { KeyboardArrowDownSharp, KeyboardArrowRightSharp, ThumbDown, ThumbDownOutlined, ThumbUp, ThumbUpOutlined } from '@mui/icons-material'
import { Box, Typography } from '@mui/material'
import React, { useState } from 'react'
import { useClient } from '../../hooks/clients.hook'
import { Opinion } from '../../models/Session.model'
import { Track } from '../../models/SpotifyItems.model'
import { truncateNameString } from '../../util/TruncateNameString'
import "./PlaylistTrackItem.css"

interface Props {
  track: Track,
  isCurrentlyPlaying: boolean,
  handleTrackOpinion: (trackId: string, opinion: Opinion) => void,
}

export const PlaylistTrackItem = (props: Props) => {
  const { track, handleTrackOpinion, isCurrentlyPlaying } = props
  const client = useClient()
  const [expanded, setExpanded] = useState<boolean>(false)
  const [opinion, setOpinion] = useState<Opinion>(client?.trackOpinions.get(track.id) ?? "neutral")

  const handleThumbIconPress = (newOpinion: Opinion) => {
    if (newOpinion === opinion) {
      handleTrackOpinion(track.id, "neutral")
      client?.trackOpinions.set(track.id, "neutral")
      setOpinion("neutral")
    }

    if (newOpinion !== opinion) {
      handleTrackOpinion(track.id, newOpinion)
      client?.trackOpinions.set(track.id, newOpinion)
      setOpinion(newOpinion)
    }
    setTimeout(() => setExpanded(false), 1500)
  }

  const VoteMenu = () => {
    return (
      <Box className={"playlistTrackItem-voteMenu"}>
        <Typography>
          Hear more like this?
        </Typography>
        <div onClick={e => handleThumbIconPress("dislikes")}>
          {opinion === "dislikes" ? <ThumbDown color={"primary"} /> : <ThumbDownOutlined />}
        </div>
        <div onClick={e => handleThumbIconPress("likes")}>
          {opinion === "likes" ? <ThumbUp color={"primary"} /> : <ThumbUpOutlined />}
        </div>
      </Box>
    )
  }

  return (
    <Box style={{ borderBottom: "0.5px solid rgba(255, 255, 255, 0.10)" }}>
      <div onClick={e => setExpanded(!expanded)} className={"playlistTrackItem-container"}>
        <Box className={"playlistTrackItem-name-artist"}>
          <img className={"playlistTrackItem-image"} src={track?.albumUrl} alt="" />
          <Box>
            <Typography variant={"h6"}>
              {track?.title ? truncateNameString(track?.title, 24) : ""}
            </Typography>
            <Typography>
              {track.artist}
            </Typography>
          </Box>
        </Box>
        <Box className={"playlistTrackItem-icons-container"}>
          {isCurrentlyPlaying ?
            <div className={"musicBars-icon"}>
              <span />
              <span />
              <span />
            </div>
            : null}
          <Box className={"playlistTrackItem-arrow-icon"}>
            {expanded ?
              <KeyboardArrowDownSharp />
              :
              <KeyboardArrowRightSharp />
            }
          </Box>
        </Box>

      </div>
      {expanded ? <VoteMenu /> : null}
    </Box>
  )
}