import SearchIcon from "@mui/icons-material/Search";
import { Box, InputAdornment, TextField, ThemeProvider } from '@mui/material'
import React from 'react'

interface Props {
  searchInput: string,
  setSearchInput: (input: string) => void,
}

export const SearchBar = (props: Props) => {
  const { searchInput, setSearchInput } = props

  return (
    <>
      <Box className="search-container" style={{ width: "100%" }}>
        <TextField
          fullWidth
          type={'text'}
          color={"primary"}
          value={searchInput}
          label={"Search for tracks, artists or playlists..."}
          onChange={e => setSearchInput(e.target.value.toLowerCase())}
          variant="filled"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
      </Box>
    </>
  )
}