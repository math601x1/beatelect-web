import React from 'react'
import "./theme.css"
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { SocketsProvider } from './context/socket.context';
import { SpotifyApiProvider } from './context/spotify.context';
import Home from './pages/Home';
import Login from './pages/Login';
import Session from './pages/Session';
import { ThemeProvider } from '@emotion/react';
import appTheme from './muiTheme';

const App = () => {

  return (
    <ThemeProvider theme={appTheme}>
      <SpotifyApiProvider>
        <SocketsProvider>
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<Login />} />
              <Route path="/home" element={<Home />} />
              <Route path="/session" element={<Session />} />
            </Routes>
          </BrowserRouter>
        </SocketsProvider>
      </SpotifyApiProvider>
    </ThemeProvider>
  );
}

export default App;
