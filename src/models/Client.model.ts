import { Opinion } from "./Session.model"

export interface Client {
  socketId: string,
  spotifyId: string,
  spotifyImageUrl: string,
  isSessionHost: boolean,
  trackOpinions: Map<string, Opinion>
  isFollowingPlaylist: boolean
}